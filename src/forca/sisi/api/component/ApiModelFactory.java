package forca.sisi.api.component;

import java.sql.ResultSet;

import org.adempiere.base.IModelFactory;
import org.compiere.model.PO;
import org.compiere.util.Env;

import forca.sisi.api.model.MWSToken;

public class ApiModelFactory implements IModelFactory{

	@Override
	public Class<?> getClass(String tableName) {
		if (tableName.equalsIgnoreCase(MWSToken.Table_Name)) {
			return MWSToken.class;
		}
		return null;
	}

	@Override
	public PO getPO(String tableName, int Record_ID, String trxName) {
		if (tableName.equalsIgnoreCase(MWSToken.Table_Name)) {
			return new MWSToken(Env.getCtx(), Record_ID, trxName);
		}
		return null;
	}

	@Override
	public PO getPO(String tableName, ResultSet rs, String trxName) {
		if (tableName.equalsIgnoreCase(MWSToken.Table_Name)) {
			return new MWSToken(Env.getCtx(), rs, trxName);
		}
		return null;
	}

}
