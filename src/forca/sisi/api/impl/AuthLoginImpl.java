package forca.sisi.api.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.compiere.model.MSession;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.util.CLogger;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.KeyNamePair;

import forca.sisi.api.filter.AuthenticationApi;
import forca.sisi.api.filter.WsLogin;
import forca.sisi.api.model.MWSToken;
import forca.sisi.api.request.ParamRequest;
import forca.sisi.api.response.ResponseData;
import forca.sisi.api.util.Constants;

public class AuthLoginImpl {
	private CLogger wslog = CLogger.getCLogger (getClass());
	public static Properties wsenv = new Properties();
	public List<Object> listdata = new ArrayList<Object>();
	ResponseData resp = new ResponseData();	
	
	public static Properties getDefaultCtx(){
		wsenv.setProperty("#AD_Language", "en_US");
		wsenv.setProperty("context", "deverp.semenindonesia.com");
		wsenv.setProperty("#AD_CLIENT_ID", "0");		
		return wsenv;
	}
	
	public ResponseData getLoginForca(String user, String password) {
//		ResponseData resp = new ResponseData();	
		getDefaultCtx();		
		
		if(user.isEmpty() || user == null || password.isEmpty() || password == null)
		{						
			resp.setMessage("Username or Password required");
			return resp;
		}
		
		KeyNamePair[] clients = null;
		
		WsLogin login = new WsLogin(wsenv);
		clients = login.getClients (user,password);
		if (clients == null){
			resp.setMessage("Username or Password inconsistent");
			return resp;
			
		}else{
			wsenv.setProperty(Constants.CTX_CLIENT, clients[0].getID());
			MUser mUser = MUser.get (wsenv, user);
			List<Object> resultdata = new ArrayList<Object>();
			Map<String, String> map = new LinkedHashMap<String, String>();
			if(mUser != null){
				map.put("ad_user_id", String.valueOf(mUser.getAD_User_ID()));
				map.put("ad_user_name", String.valueOf(mUser.getName()));				
				map.put("ad_client_id", String.valueOf(mUser.getAD_Client_ID()));
				map.put("ad_org_id", String.valueOf(mUser.getAD_Org_ID()));
				map.put("salesrep_ID", String.valueOf(mUser.getAD_User_ID()));
				resultdata.add(map);
			}			
			
			resp.setCodestatus("S");
			resp.setMessage("Berhasil, Data Ditemukan");
			resp.setResultdata(resultdata);
			return resp;		
		}	
	}
	
	public ResponseData getClientForca(String user, String password) {
		try {					
			if(user.equals("") || user == null || password.equals("") || password == null)
			{
				resp.setMessage("Username or Password required");
				return resp;
			}
			
			KeyNamePair[] clients = null;
			
			WsLogin login = new WsLogin(wsenv);
			
			clients = login.getClients (user,password);
			if (clients == null){
				//resp.setCodestatus("E");
				resp.setMessage("Username and Password inconsistent");
				return resp;			
			}else{
				Map<String, String> map = new LinkedHashMap<String, String>();
				//for(int i=0; i<clients.length; i++){				
					map.put("ad_client_id", clients[0].getID());
					map.put("client_name", clients[0].getName());
				//}
				List<Object> resultdata = new ArrayList<Object>();
				resultdata.add(map);
				resp.setCodestatus("S");
				resp.setMessage("Berhasil, Data Ditemukan");
				resp.setResultdata(resultdata);
				
				return resp;		
			}		
		} catch (Exception e) {
			resp.setMessage(e.getMessage());
			return resp;
		}
	}
	
	public ResponseData getRoleForca(ParamRequest param) {
		String user = param.getUsername();
		String password = param.getPassword();
		Integer ad_client_id = param.getAd_client_id();
		//Integer ad_user_id = param.getAd_user_id();
		
		
		if (!DB.isConnected())
		{
			//resp.setCodestatus("E");
			resp.setMessage("No Database Connection");
			return resp;
		}
		//|| ad_user_id == null
		if(user == null //|| password == null 
				|| ad_client_id == null)
		{
			//resp.setCodestatus("E");
			resp.setMessage("Parameter required");
			return resp;
		}
		
		KeyNamePair clients = new KeyNamePair(ad_client_id, user);
		KeyNamePair[] roles = null;
		getDefaultCtx();
		//wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
		wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
		wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
		
		WsLogin login = new WsLogin(wsenv);
		
		//clients = login.getClients (user,password);
		
		roles = login.getRoles(user, clients);
		
		if (roles == null){
			resp.setMessage("Roles not found for this user");
			return resp;			
		}else{
			//roles = login.getRoles(APP_USER, clients[0]);
	
			List<Object> resultdata = new ArrayList<Object>();
			
			for(int i=0; i<roles.length; i++){				
				Map<String, String> map = new LinkedHashMap<String, String>();
				map.put("ad_role_id", roles[i].getID());
				map.put("role_name", roles[i].getName());
				resultdata.add(map);
			}
			
			//setUserID(wsc.ctx, clients[0].getKey());
			resp.setCodestatus("S");
			resp.setMessage("Berhasil, Data Ditemukan");
			resp.setResultdata(resultdata);

			
			return resp;		
		}				
	}
	
	public ResponseData getOrgForca(ParamRequest param) {
		ResponseData resp = new ResponseData();	
		String user = param.getUsername();
		//String password = param.getPassword();
		Integer ad_client_id = param.getAd_client_id();
		//Integer ad_user_id = param.getAd_user_id();
		Integer ad_role_id = param.getAd_role_id();
		
		
		if (!DB.isConnected())
		{
			//resp.setCodestatus("E");
			resp.setMessage("No Database Connection");
			return resp;
		}
		if(user == null //|| password == null || ad_user_id == null
				|| ad_client_id == null || ad_role_id== null)
		{
			//resp.setCodestatus("E");
			resp.setMessage("Parameter required");
			return resp;
		}
		
		//KeyNamePair clients = new KeyNamePair(ad_client_id, "Nama Client");
		KeyNamePair roles = new KeyNamePair(ad_role_id, "");
		KeyNamePair[] orgs = null;
		getDefaultCtx();
		//wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
		wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
		wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
		wsenv.setProperty(Constants.CTX_ROLE, String.valueOf(ad_role_id));
		
		WsLogin login = new WsLogin(wsenv);
		orgs    = login.getOrgs(roles);
		
		if (orgs == null){
			//resp.setCodestatus("E");
			resp.setMessage("Organization not found for this role");
			
			return resp;			
		}else{
			//roles = login.getRoles(APP_USER, clients[0]);
	
			List<Object> resultdata = new ArrayList<Object>();			
			for(int i=0; i<orgs.length; i++){				
				Map<String, String> map = new LinkedHashMap<String, String>();
				map.put("ad_org_id", orgs[i].getID());
				map.put("org_name", orgs[i].getName());
				resultdata.add(map);
			}
			
			
			resp.setCodestatus("S");
			resp.setMessage("Berhasil, Data Ditemukan");
			resp.setResultdata(resultdata);
			return resp;		
		}				
	}
	
	public ResponseData getWarehouseForca(ParamRequest param) {
		//ResponseData resp = new ResponseData();	
		String user = param.getUsername();
		//String password = param.getPassword();
		Integer ad_client_id = param.getAd_client_id();
		//Integer ad_user_id = param.getAd_user_id();
		//Integer ad_role_id = param.getAd_role_id();
		Integer ad_org_id = param.getAd_org_id();
		
		
		if (!DB.isConnected())
		{
			//resp.setCodestatus("E");
			resp.setMessage("No Database Connection");
			return resp;
		}
		if(user == null //|| password == null || ad_user_id == null
				|| ad_client_id == null || ad_org_id== null)
		{
			//resp.setCodestatus("E");
			resp.setMessage("Parameter required");
			return resp;
		}
		
		//KeyNamePair clients = new KeyNamePair(ad_client_id, "Nama Client");
		//KeyNamePair roles = new KeyNamePair(ad_role_id, "Nama Role");
		KeyNamePair[] whouse = null;
		
		getDefaultCtx();
		//wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
		wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
		wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
		wsenv.setProperty(Constants.CTX_ORG, String.valueOf(ad_org_id));
		Env.setCtx(wsenv);
		
		WsLogin login = new WsLogin(wsenv);
		whouse = login.getWarehouses(new KeyNamePair(ad_org_id, ""));
		
		if (whouse == null){
			//resp.setCodestatus("E");
			resp.setMessage("Organization not found for this role");
			return resp;			
		}else{
			//roles = login.getRoles(APP_USER, clients[0]);
	
			List<Object> resultdata = new ArrayList<Object>();
			
			for(int i=0; i<whouse.length; i++){				
				Map<String, String> map = new LinkedHashMap<String, String>();
				map.put("m_warehouse_id", whouse[i].getID());
				map.put("warehouse_name", whouse[i].getName());
				resultdata.add(map);
			}
			
			
			resp.setCodestatus("S");
			resp.setMessage("Berhasil, Data Ditemukan");
			resp.setResultdata(resultdata);
			return resp;		
		}				
	}
	public ResponseData getTokenApi(ParamRequest param) {
		
		String username = param.getUsername();
		String password = param.getPassword();
		Integer ad_client_id = param.getAd_client_id();
		//Integer ad_user_id = param.getAd_user_id();
		Integer ad_role_id = param.getAd_role_id();
		Integer ad_org_id = param.getAd_org_id();
		Integer m_warehouse_id = param.getM_warehouse_id();
		//Integer c_bpartner_id = param.getC_bpartner_id();
		KeyNamePair[] users = null;
		
		if (!DB.isConnected())
		{
			resp.setMessage("No Database Connection");
			return resp;
		}
		if(username == null || password == null //|| ad_user_id == null
				|| ad_client_id == null || ad_org_id== null || ad_role_id== null)
		{
			resp.setMessage("Parameter required");
			return resp;
		}
		try {
			Timestamp exptime = new Timestamp(System.currentTimeMillis());
			 Calendar cal = Calendar.getInstance(); 
			 cal.setTimeInMillis(exptime.getTime());
			 cal.add(Calendar.MONTH, 1);
			 exptime = new Timestamp(cal.getTime().getTime());
			
			WsLogin login = new WsLogin(getDefaultCtx());
			users = login.getAD_User(username, password);
			AuthenticationApi.setContext(Integer.parseInt(users[0].getID()), ad_client_id,
					ad_role_id, ad_org_id, m_warehouse_id);
			
			MWSToken wstoken = new MWSToken(Env.getCtx(), 0, null);
			wstoken.setAD_Org_ID(ad_org_id);
			wstoken.setAD_Role_ID(ad_role_id);
			wstoken.setAD_User_ID(Integer.parseInt(users[0].getID()));
			wstoken.setM_Warehouse_ID(m_warehouse_id);
			//wstoken.setC_BPartner_ID(c_bpartner_id);
			wstoken.setExpiredDate(exptime); //expired 1 bulan
			if(wstoken.save()){
				Map<String, Object> map = new LinkedHashMap<String,Object>();
				map.put("token", wstoken.getWS_Token_UU());
				map.put("ad_user_id", users[0].getID());
				map.put("expired", wstoken.getExpiredDate().toString());
				listdata.add(map);
				resp.setCodestatus("S");
				resp.setMessage("Berhasil");
				resp.setResultdata(listdata);
			}else{
				resp.setMessage("Cannot Create Token, Check Parameter Require");
			}
			
			
		} catch (Exception e) {
			resp.setMessage(e.getMessage());
		}
		
		//KeyNamePair clients = new KeyNamePair(ad_client_id, "Nama Client");
		//KeyNamePair roles = new KeyNamePair(ad_role_id, "Nama Role");
//		KeyNamePair[] whouse = null;
//		
//		getDefaultCtx();
//		//wsenv.setProperty(Constants.CTX_USER, String.valueOf(ad_user_id));
//		wsenv.setProperty(Constants.CTX_CLIENT, String.valueOf(ad_client_id));
//		wsenv.setProperty(Constants.CTX_USER_NAME, String.valueOf(user));
//		wsenv.setProperty(Constants.CTX_ORG, String.valueOf(ad_org_id));
//		Env.setCtx(wsenv);
//		
//		WsLogin login = new WsLogin(wsenv);
//		whouse = login.getWarehouses(new KeyNamePair(ad_org_id, ""));
//		
//		if (whouse == null){
//			//resp.setCodestatus("E");
//			resp.setMessage("Organization not found for this role");
//			return resp;			
//		}else{
//			//roles = login.getRoles(APP_USER, clients[0]);
//	
//			List<Object> resultdata = new ArrayList<Object>();
//			Map<String, String> map = new LinkedHashMap<String, String>();
//			for(int i=0; i<whouse.length; i++){				
//				map.put(whouse[i].getID(), whouse[i].getName());
//			}
//			resultdata.add(map);
//			
//			resp.setCodestatus("S");
//			resp.setMessage("Berhasil, Data Ditemukan");
//			resp.setResultdata(resultdata);
//					
//		}		
		return resp;
	}
	
	public ResponseData refreshToken(ParamRequest param) {
		
		String token = param.getToken();
		
		if (!DB.isConnected())
		{
			resp.setMessage("No Database Connection");
			return resp;
		}
		if(token == null)
		{
			resp.setMessage("Parameter required");
			return resp;
		}
		try {
			Timestamp exptime = new Timestamp(System.currentTimeMillis());
			 Calendar cal = Calendar.getInstance(); 
			 cal.setTimeInMillis(exptime.getTime());
			 cal.add(Calendar.MONTH, 1);
			 exptime = new Timestamp(cal.getTime().getTime());
			 
			AuthenticationApi.setContextByToken(token);
			
			String prmtr = "WS_Token_UU = '"+token+"' ";
			Query q = new Query(getDefaultCtx(), MWSToken.Table_Name, prmtr, null);	
			q.setOnlyActiveRecords(true);
			MWSToken mwstoken = q.first();
			
			if(mwstoken.getWS_Token_ID()>0){
				MWSToken newtoken = new MWSToken(Env.getCtx(), 0, null);
				newtoken.setAD_Org_ID(mwstoken.getAD_Client_ID());
				newtoken.setAD_Role_ID(mwstoken.getAD_Role_ID());
				newtoken.setAD_User_ID(mwstoken.getAD_User_ID());
				newtoken.setM_Warehouse_ID(mwstoken.getM_Warehouse_ID());
				newtoken.setExpiredDate(exptime); //expired 1 bulan
				if(newtoken.save()){
					Map<String, Object> map = new LinkedHashMap<String,Object>();
					map.put("token", newtoken.getWS_Token_UU());
					map.put("ad_client_id", newtoken.getAD_Client_ID());
					map.put("ad_role_id", newtoken.getAD_Role_ID());
					map.put("ad_org_id", newtoken.getAD_Org_ID());
					map.put("ad_user_id", newtoken.getAD_User_ID());
					map.put("expired", newtoken.getExpiredDate());
					listdata.add(map);
					resp.setCodestatus("S");
					resp.setMessage("Berhasil");
					resp.setResultdata(listdata);
				}else{
					resp.setMessage("Cannot Create Token, Check Parameter Require");
				}
			}else {
				resp.setMessage("No Valid Token");
			}
		} catch (Exception e) {
			resp.setMessage(e.getMessage());
		}		
		return resp;
	}
	
	public ResponseData infoToken(ParamRequest param) {
		
		String token = param.getToken();		
		if (!DB.isConnected())
		{
			resp.setMessage("No Database Connection");
			return resp;
		}
		if(token == null)
		{
			resp.setMessage("Parameter required");
			return resp;
		}
		try {						 
			AuthenticationApi.setContextByToken(token);
			
			String prmtr = "WS_Token_UU = '"+token+"' ";
			Query q = new Query(Env.getCtx(), MWSToken.Table_Name, prmtr,	null);	
			q.setOnlyActiveRecords(true);
			MWSToken mwstoken = q.first();
			
			if(mwstoken != null){
				Map<String, Object> map = new LinkedHashMap<String,Object>();
				map.put("token", mwstoken.getWS_Token_UU());
				map.put("ad_client_id", mwstoken.getAD_Client_ID());
				map.put("ad_role_id", mwstoken.getAD_Role_ID());
				map.put("ad_org_id", mwstoken.getAD_Org_ID());
				map.put("ad_user_id", mwstoken.getAD_User_ID());
				map.put("expired", mwstoken.getExpiredDate());
				map.put("hit", mwstoken.getHit());
				map.put("success", mwstoken.getSuccess());
				map.put("error", mwstoken.getErrorRequest());
				listdata.add(map);
				resp.setCodestatus("S");
				resp.setMessage("Berhasil");
				resp.setResultdata(listdata);
			}else{
				resp.setMessage("Cannot Find Token");
			}
		} catch (Exception e) {
			resp.setMessage(e.getMessage());
		}		
		return resp;
	}
	
	public static void sesionLogout(){
		MSession sesion = MSession.get(Env.getCtx(), false);
		sesion.logout();
	}
}
