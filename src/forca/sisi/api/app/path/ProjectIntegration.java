package forca.sisi.api.app.path;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.stereotype.Component;

import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiImplicitParam;
import com.wordnik.swagger.annotations.ApiImplicitParams;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import forca.sisi.api.filter.AuthenticationApi;
import forca.sisi.api.impl.CrmCormaImpl;
import forca.sisi.api.request.ParamRequest;
import forca.sisi.api.response.ResponseData;

@Component
@Path("/project/formdata")
@Api(position=5, value="Project-Integration", description="Project Integration Forca")
public class ProjectIntegration {
	ResponseData respon = new ResponseData();
	ParamRequest param = new ParamRequest();
	
	  @POST
	  @Path("/get-project-consume")
	  @Consumes(MediaType.MULTIPART_FORM_DATA)
	  @Produces(MediaType.APPLICATION_JSON)
	  @ApiOperation(position=0, httpMethod="POST", value="form-data", notes="Upsert Business Partner ")
	  @ApiImplicitParams({@ApiImplicitParam(name="Forca-Token", value="Token Forca", 
	  			required=true, dataType="string", paramType="header")})
	  public Response getProjectConsume(@ApiParam(required=true) @FormParam("c_project_id") int c_project_id,
			  	@ApiParam(value="diisi jika update", required=false) @FormParam("c_bpartner_id") int c_bpartner_id, 
	  			@ApiParam(value="diisi jika update", required=false) @FormParam("c_location_id") int c_location_id, 
	  			@ApiParam(required=true) @FormParam("name_partner") String name_partner, 
	  			@ApiParam(required=false) @FormParam("referenceno") String referenceno, 
	  			@ApiParam(required=false) @FormParam("c_country_id") int c_country_id, 
	  			@ApiParam(required=false) @FormParam("c_region_id") int c_region_id, 
	  			@ApiParam(required=false) @FormParam("c_city_id") int c_city_id)
	  {
	    ParamRequest param = new ParamRequest();
	    param.setC_bpartner_id(Integer.valueOf(c_bpartner_id));
	    
	    param.setC_location_id(Integer.valueOf(c_location_id));
	    param.setReferenceno(referenceno);
	    
	    param.setName_partner(name_partner);
	    
	    if (c_country_id > 0) {
	      param.setC_country_id(Integer.valueOf(c_country_id));
	    } else {
	      param.setC_country_id(Integer.valueOf(209));
	    }
	    param.setC_region_id(Integer.valueOf(c_region_id));
	    param.setC_city_id(Integer.valueOf(c_city_id));
	    
	    CrmCormaImpl impl = new CrmCormaImpl();
	    this.respon = impl.actSetPartner(param);
	    AuthenticationApi.countErrorSukses(this.respon);
	    return Response.status(Response.Status.OK).entity(this.respon).build();
	  }
}
