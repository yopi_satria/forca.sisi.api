package forca.sisi.api.app.path;

//import java.security.cert.CertPathValidatorException.Reason;
//import java.util.ArrayList;
//import java.util.LinkedHashMap;
//import java.util.List;
//import java.util.Map;



import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
//import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
//import javax.ws.rs.core.Response.StatusType;









import com.sun.jersey.api.container.filter.LoggingFilter;
import com.wordnik.swagger.annotations.*;

//import org.compiere.util.Env;
//import org.json.JSONArray;
//import org.springframework.http.ResponseEntity;
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//import forca.erp.api.component.Constants;






import forca.sisi.api.filter.RestFilter;
import forca.sisi.api.request.ParamRequest;
import forca.sisi.api.response.ResponseData;


@Component
@Path("/test")
@Api(position = 1,value="Test", description ="Testing RESTful API" )
public class TestServices {
//	@Autowired
	
	ParamRequest pa;
	LoggingFilter loggingFilter;

	@GET
	@Path("/hello")	
	@ApiOperation(position=1, httpMethod ="GET", value = "Test Hello with method GET",
    	notes = "Multiple status values can be provided with comma seperated strings")	
	@ApiResponses(value ={
			@ApiResponse(code=200, message="OK"),
			@ApiResponse(code=500, message="Something Wrong")
	})
	public Response hello() {
		HttpServletRequest httpServletRequest = RestFilter.httpServletRequest;
		//String authCredentials = httpServletRequest.getHeader(Constants.AUTH_HEADER);
		
		return Response.status(200).entity("hello, kamu mengakses Forca App \n"
				+" path "+httpServletRequest.getPathInfo()).build();
	}
	
	@GET
	@Path("/hello-notoken")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(position=4,httpMethod ="GET", value = "Test Hello without Forca-Token ",
    	notes = "Test API without Forca-Token")
	@ApiResponses(value ={
			@ApiResponse(code=200, message="OK"),
			@ApiResponse(code=500, message="Something Wrong")
	})
	public Response hellonotoken() {

		//String result = transactionBo.save();
		return Response.status(200).entity("Hello from Forca Application with No Token submission").build();
	}
	
	@GET
	@Path("/xmltest")
	@Produces(MediaType.APPLICATION_XML)
	@ApiOperation(position=2,httpMethod ="GET", value = "Test Xml ",
		notes = "Test API return xml value")
	@ApiResponses(value ={
			@ApiResponse(code=200, message="OK"),
			@ApiResponse(code=500, message="Something Wrong")
	})
	public Response xmltest() {
		ResponseData responseData  =  new ResponseData();
		 responseData.setMessage("pengembalian xml");
		 return Response.status(Status.OK).entity(responseData).build();
//		return "<?xml version=\"1.0\"?>" + "<say><hello> Web Service"
//				+ "</hello></say>";
	}
	
	 @GET 
	 @Path("/jsontest")
	 @Produces(MediaType.APPLICATION_JSON)
	 @ApiOperation(position=3,httpMethod ="GET", value = "Test Json ",
		notes = "Test API return json value")
	 @ApiResponses(value ={
			@ApiResponse(code=200, message="OK"),
			@ApiResponse(code=500, message="Something Wrong")
	 })
	 public Response testJson() {
		 ResponseData responseData  =  new ResponseData();
		 responseData.setMessage("pengembalian json");
		 return Response.status(Status.OK).entity(responseData).build();
	 }
	 
	@GET
	@Path("/hello-token")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@ApiOperation(position=5,httpMethod ="GET", value = "Test Hello with Forca-Token ",
    	notes = "Test working your token")
	@ApiImplicitParams({
	    @ApiImplicitParam(name = "Forca-Token", value = "Token Forca", required = true, dataType = "string", paramType = "header")
	  })
	@ApiResponses(value ={
			@ApiResponse(code=200, message="OK"),
			@ApiResponse(code=401, message="UnAuthorize Token")
	})
	public Response hellotoken() {	
		//String result = transactionBo.save();
		return Response.status(200).entity("Hello from Forca with Token header").build();
	}
}
