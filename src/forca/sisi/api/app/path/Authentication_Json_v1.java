package forca.sisi.api.app.path;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.stereotype.Component;

import forca.sisi.api.filter.AuthenticationApi;
import forca.sisi.api.impl.AuthLoginImpl;
import forca.sisi.api.request.ParamRequest;
import forca.sisi.api.response.ResponseData;

@Component
@Path("/authentication/json")
public class Authentication_Json_v1 {
	AuthenticationApi auth;
	ResponseData respon = new ResponseData();
	//ParamRequest param = new ParamRequest();
	
	@POST
	@Path("/getclients")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/json")
	public Response getClients(ParamRequest param) {
		
		AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
		respon = apiLoginImpl.getClientForca(param.getUsername(), param.getPassword());		
		return Response.status(Status.OK).entity(respon).build();
	}

	@POST
	@Path("/getroles")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRoles(ParamRequest param) {
				
		AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
		respon = apiLoginImpl.getRoleForca(param);
		return Response.status(Status.OK).entity(respon).build();
	}

	@POST
	@Path("/getorgs")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/json")
	public Response getOrgs(ParamRequest param) {
			
		AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
		respon = apiLoginImpl.getOrgForca(param);
		return Response.status(Status.OK).entity(respon).build();
	}

	@POST
	@Path("/getwarehouses")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/json")
	public Response getWarehouses(ParamRequest param) {
		
		
		AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
		respon = apiLoginImpl.getWarehouseForca(param);
		return Response.status(Status.OK).entity(respon).build();
		
	}

	@POST
	@Path("/gettoken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response getToken(ParamRequest param) {
				
		AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
		respon = apiLoginImpl.getTokenApi(param);
		return Response.status(Status.OK).entity(respon).build();		
	}
	
	//refreshtoken
	@POST
	@Path("/refreshtoken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response refreshToken(ParamRequest param) {
			
		AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
		respon = apiLoginImpl.refreshToken(param);
		return Response.status(Status.OK).entity(respon).build();
		
	}
	
	//info token
	@POST
	@Path("/infotoken")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response infoToken(ParamRequest param) {		
		AuthLoginImpl apiLoginImpl = new AuthLoginImpl();
		respon = apiLoginImpl.infoToken(param);
		return Response.status(Status.OK).entity(respon).build();		
	}
}
