package forca.sisi.api.app;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

//import com.wordnik.swagger.jaxrs.config.BeanConfig;

//import com.wordnik.swagger.config.ConfigFactory;
//import com.wordnik.swagger.config.ScannerFactory;
//import com.wordnik.swagger.config.SwaggerConfig;
//import com.wordnik.swagger.jaxrs.config.BeanConfig;
//import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
//import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
//import com.wordnik.swagger.reader.ClassReaders;





import com.wordnik.swagger.config.ConfigFactory;
import com.wordnik.swagger.config.ScannerFactory;
import com.wordnik.swagger.config.SwaggerConfig;
import com.wordnik.swagger.jaxrs.reader.DefaultJaxrsApiReader;
//import com.wordnik.swagger.jaxrs.config.BeanConfig;
import com.wordnik.swagger.jaxrs.config.DefaultJaxrsScanner;
import com.wordnik.swagger.model.ApiInfo;
import com.wordnik.swagger.reader.ClassReaders;

import forca.sisi.api.app.path.Authentication_FormData_v1;
import forca.sisi.api.app.path.CrmCorma_Form;
import forca.sisi.api.app.path.Master_FormData_v1;
import forca.sisi.api.app.path.TestServices;

//@javax.ws.rs.ApplicationPath("apiforca")
public class ServerAplication extends Application {
		
	@Override
	public Set<Class<?>> getClasses() {
		Set<Class<?>> s = new HashSet<Class<?>>();
//		s.add(TestServices.class);
//		s.add(Authentication_FormData_v1.class);
//		s.add(Master_FormData_v1.class);
		
		//swagger Core Plugin
//		s.add(com.wordnik.swagger.jersey.listing.ApiListingResource.class);		
//        s.add(com.wordnik.swagger.jersey.listing.JerseyApiDeclarationProvider.class);
//        s.add(com.wordnik.swagger.jersey.listing.ApiListingResourceJSON.class);
//        s.add(com.wordnik.swagger.jersey.listing.JerseyResourceListingProvider.class);
        
        s.add(com.wordnik.swagger.jaxrs.listing.ApiListingResource.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ApiDeclarationProvider.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ApiListingResourceJSON.class);
        s.add(com.wordnik.swagger.jaxrs.listing.ResourceListingProvider.class);

       
        addResourcePath(s);
        swaggerConfiguration();
        
		return s;
	}
	
	private void addResourcePath(Set<Class<?>> resource){
		resource.add(TestServices.class);
		resource.add(Authentication_FormData_v1.class);
		resource.add(Master_FormData_v1.class);
		resource.add(CrmCorma_Form.class);
	}
	
	private void swaggerConfiguration( ) {
      SwaggerConfig swaggerConfig = new SwaggerConfig( );
      ConfigFactory.setConfig( swaggerConfig );

      swaggerConfig.setBasePath( "/apiforca/ws" );
      ApiInfo info = new ApiInfo(
              "Forca API Documentation",
              "RESTful API from Forca ERP<br>"
              + "Please Contact administrator if you want to access it !!!",
              "http://sisi.id/",
              "administrator@forca.id",
              "Apache 2.0",
              "http://www.apache.org/licenses/LICENSE-2.0.html"
          );
      swaggerConfig.setApiVersion( "0.0.1" ); 

      swaggerConfig.setApiInfo(info);
      
      //swaggerConfig.setSwaggerVersion("2.0"); define manual
      ScannerFactory.setScanner(new DefaultJaxrsScanner() );
      ClassReaders.setReader( new DefaultJaxrsApiReader( ) );
   }
}
