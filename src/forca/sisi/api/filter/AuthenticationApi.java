package forca.sisi.api.filter;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.ws.rs.core.Response.Status;

import org.compiere.model.MSession;
import org.compiere.model.Query;
import org.compiere.util.DB;
import org.compiere.util.Env;

import forca.sisi.api.model.MWSToken;
import forca.sisi.api.model.X_WS_Token;
import forca.sisi.api.response.ResponseData;
import forca.sisi.api.util.Constants;

public class AuthenticationApi {
	
	//Diperbolehkan tanpa header token
	List<String> urlprefix = Arrays.asList(
			"/test/",
			"/authentication/",
			"/api-docs",
			"/swaggerui"
			);

	public static Properties wsctx = new Properties();
	Status status = Status.OK;
	
	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
			
	public AuthenticationApi() {
		super();
	}
	
	public Boolean nonHeaderAuth(String path) {
		Boolean valid = false;
		
		for (String url : urlprefix){
			if (path.contains(url)){
				valid = true;
			}
		}
				
//		if(uri.contains(path)){
//			valid = true;
//		}
		return valid;
	}
	
//	public static void setContextForca(String token){
//		
//		
//		wsctx.setProperty(Constants.CTX_CLIENT, "100000");
//		wsctx.setProperty(Constants.CTX_ORG,"100001");
//		wsctx.setProperty(Constants.CTX_ROLE,"100002");			
//		wsctx.setProperty(Constants.CTX_USER,"100003");
//		wsctx.setProperty(Constants.CTX_WAREHOUSE, "100004");			
//		Env.setCtx(wsctx);
//	}
	
	public static Properties getDefaultCtx(){
		//wsctx.setProperty("context", "deverp.semenindonesia.com");
		wsctx.setProperty(Constants.CTX_LANGUAGE, "en_US");
		wsctx.setProperty(Constants.CTX_CLIENT, "0");		
		return wsctx;
	}
	public static void setContext(Integer user_id, Integer client_id,
			Integer role_id, Integer org_id, Integer warehouse_id){
		
		wsctx.setProperty(Constants.CTX_LANGUAGE, "en_US");
		wsctx.setProperty(Constants.CTX_USER, user_id.toString());	
		wsctx.setProperty(Constants.CTX_CLIENT, client_id.toString());	
		wsctx.setProperty(Constants.CTX_ROLE, role_id.toString());	
		wsctx.setProperty(Constants.CTX_ORG, org_id.toString());		
		wsctx.setProperty(Constants.CTX_WAREHOUSE, warehouse_id.toString());		
		Env.setCtx(wsctx);
		//return wsctx;
	}
	

	public static void setContextByToken(String token){
		StringBuilder sql = new StringBuilder();
		PreparedStatement statement = null;
		ResultSet rs = null;
		try {
			sql.append("select * from ws_token where ws_token_uu ilike ? fetch first 1 row only");
			statement = DB.prepareStatement(sql.toString(), null);
			statement.setString(1, token);
			rs = statement.executeQuery();
			while (rs.next()) {
				wsctx.setProperty(Constants.CTX_CLIENT,rs.getString("ad_client_id"));
				wsctx.setProperty(Constants.CTX_ORG,rs.getString("ad_org_id"));
				wsctx.setProperty(Constants.CTX_ROLE,rs.getString("ad_role_id"));			
				wsctx.setProperty(Constants.CTX_USER,rs.getString("ad_user_id"));
				//wsctx.setProperty(Constants.CTX_WAREHOUSE, rs.getString("m_warehouse_id"));	
				wsctx.setProperty(Constants.CTX_TOKEN, rs.getString("ws_token_uu"));
				Env.setCtx(wsctx);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DB.close(rs, statement);
			rs = null;
			statement = null;
		}
	}
	
	public static void logoutSession(){
		MSession msesion = MSession.get(Env.getCtx(), false);
		msesion.logout();
	}

//	public Boolean validToken(String token){
//		Boolean valtoken = false;	
//		Boolean validasi = null;
//		try {
//			validasi = validasiToken(token);
//			//Map<String, String>[] maps = tkn.getMaps(uuid);
//			if (validasi) {
//				valtoken = true;
//				setStatus(Status.OK);	
//			}else {
//				setStatus(Status.UNAUTHORIZED);	
//			}
//		} catch (Exception e) {
//			setStatus(Status.BAD_REQUEST);	
//		}		
//		return valtoken;
//	}
	
	public Boolean validasiToken(String token){
		Boolean valid = false;			
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Env.setCtx(getDefaultCtx());
		try {
			
			String sql= "select * from ws_token "
						+"where WS_Token_UU = '"+token+"' "
						+ "and expireddate >= now() fetch first rows only";
			stmt = DB.prepareStatement(sql, null);
			rs = stmt.executeQuery();
			if(rs.next()){
				MWSToken mwstoken = new MWSToken(Env.getCtx(),rs, null);
		
				if(mwstoken.getWS_Token_ID() > 0)
				{	valid = true;					
				}else{
					valid = false;	
				}
			}else{
				valid = false;	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			valid = false;
		} finally {
			DB.close(rs, stmt);
			rs = null;
			stmt = null;
		}
		return valid;
	}
	
	public static void countHitToken(String token){
		//Boolean valid = false;		
		try {
			String prmtr = "WS_Token_UU = '"+token+"'";
			Query q = new Query(Env.getCtx(), X_WS_Token.Table_Name, prmtr,	null);	
			q.setOnlyActiveRecords(true);
			MWSToken mwstoken = q.first();
			
			if(mwstoken != null)
			{	
				Long valhit = mwstoken.getHit().longValue();
				valhit++;
				mwstoken.setHit(new BigDecimal(valhit));
				mwstoken.saveEx();
			}
			//valid=true;
		} catch (Exception e) {
			e.printStackTrace();
			//valid = false;
		} 
		//return valid;
	}
	public static void countErrorSukses(ResponseData respon){
		if(respon.getCodestatus().equals("S")){
			setErrorSuksesToken(true);
		}else{
			setErrorSuksesToken(false);
		}
	}
	
	public static void setErrorSuksesToken(Boolean sukses){
		String xtoken = Env.getContext(Env.getCtx(), Constants.CTX_TOKEN);
		//Boolean valid = false;		
		try {
			String prmtr = "WS_Token_UU = '"+xtoken+"'";
			Query q = new Query(Env.getCtx(), MWSToken.Table_Name, prmtr,	null);	
			q.setOnlyActiveRecords(true);
			MWSToken mwstoken = q.first();
			
			if(mwstoken != null)
			{	
				if(sukses){
					Long valsukses = new Long(mwstoken.getSuccess().longValue());
					valsukses++;
					mwstoken.setSuccess(new BigDecimal(valsukses));
					mwstoken.saveEx();
				}else{
					Long valerror = new Long(mwstoken.getErrorRequest().longValue());
					valerror++;
					mwstoken.setErrorRequest(new BigDecimal(valerror));
					mwstoken.saveEx();
				}
				
			}
			//valid=true;
		} catch (Exception e) {
			e.printStackTrace();
			//valid = false;
		} 
		//return valid;
	}
//	
//	public Boolean countErrorToken(String token){
//		Boolean valid = false;		
//		try {
//			String prmtr = "WS_Token_UU = '"+token+"'";
//			Query q = new Query(Env.getCtx(), MWSToken.Table_Name, prmtr,	null);	
//			q.setOnlyActiveRecords(true);
//			MWSToken mwstoken = q.first();
//			BigDecimal eror = new BigDecimal(0);
//			
//			if(mwstoken != null)
//			{	
//				eror = eror.add(mwstoken.getErrorRequest());
//				eror = eror.add(new BigDecimal(1));
//				mwstoken.setErrorRequest(eror);
//				mwstoken.saveEx();
//			}
//			valid=true;
//		} catch (Exception e) {
//			valid = false;
//		} 
//		return valid;
//	}
//	public Boolean countSuccessToken(String token){
//		Boolean valid = false;		
//		try {
//			String prmtr = "WS_Token_UU = '"+token+"'";
//			Query q = new Query(Env.getCtx(), MWSToken.Table_Name, prmtr,	null);	
//			q.setOnlyActiveRecords(true);
//			MWSToken mwstoken = q.first();
//			BigDecimal sukses = new BigDecimal(0);
//			
//			if(mwstoken != null)
//			{	
//				sukses = sukses.add(mwstoken.getHit());
//				sukses = sukses.add(new BigDecimal(1));
//				mwstoken.setSuccess(sukses);
//				mwstoken.saveEx();
//			}
//			valid=true;
//		} catch (Exception e) {
//			valid = false;
//		} 
//		return valid;
//	}
	
//	public boolean basicAuthenticate(String credential) {
//		if (null == credential) {
//			return false;
//		}
//		// header value format will be "Basic encodedstring" for Basic
//		// authentication. Example "Basic YWRtaW46YWRtaW4="
//		final String encodedUserPassword = credential.replaceFirst("Basic" + " ", "");
//		String usernameAndPassword = null;
////		try {
////			byte[] decodedBytes = new BASE64Decoder().decodeBuffer(encodedUserPassword);
////			usernameAndPassword = new String(decodedBytes, "UTF-8");
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
//		//final StringTokenizer tokenizer = new StringTokenizer(usernameAndPassword, ":");
//		//final String username = tokenizer.nextToken();
//		//final String password = tokenizer.nextToken();
//
//		// we have fixed the userid and password as admin
//		// call some UserService/LDAP here
//		boolean authenticationStatus = "admin".equals(username) && "admin".equals(password);
//		return authenticationStatus;
//	}
}
