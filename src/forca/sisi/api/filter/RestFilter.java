package forca.sisi.api.filter;

import java.io.IOException;







import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.compiere.util.Env;
import org.springframework.stereotype.Component;

import forca.sisi.api.request.ParamRequest;
import forca.sisi.api.util.Constants;

@Component
public class RestFilter implements Filter {
	
	ParamRequest param ;
	public static HttpServletRequest httpServletRequest;
		
	@Override
	public void destroy() {
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
			FilterChain filterChain) throws IOException, ServletException {
		
		        
        if (servletRequest instanceof HttpServletRequest) {
			httpServletRequest = (HttpServletRequest) servletRequest;
			
			//diperlukan jika memakai authorization header
			//String authCredentials = httpServletRequest
			//		.getHeader(Constants.AUTH_HEADER);
			
			final HttpServletResponse response = (HttpServletResponse) servletResponse;
	        response.setHeader("Access-Control-Allow-Origin", "*");
	        response.setHeader("Access-Control-Allow-Credentials", "true");
	        response.setHeader("Access-Control-Allow-Methods", " GET, POST");//GET, POST, DELETE, PUT, PATCH, OPTIONS
	        response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, x-auth-token, "
	                + "Content-Type, api_key, Access-Control-Request-Method,"
	                + "Authorization, Access-Control-Request-Headers");
	        response.setHeader("Copyright-Apps", "Sinergi Informatika Semen Indonesia");
	        
			String authToken = httpServletRequest
					.getHeader(Constants.AUTH_FORCA_TOKEN);
			
			//implement dependancy injection
			AuthenticationApi authenticationService = new AuthenticationApi();
			
			//check header token
			if(authToken == null || authToken.equals("")){
				if(authenticationService.nonHeaderAuth(httpServletRequest.getPathInfo())){
					filterChain.doFilter(servletRequest, response);
				}else{
					if (servletResponse instanceof HttpServletResponse) {
						HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
						httpServletResponse
								.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED); //405
					}
				}
			}else{
				if(authenticationService.validasiToken(authToken)){
					AuthenticationApi.setContextByToken(authToken);
					if(Env.getAD_Client_ID(Env.getCtx())>0)		
					{	AuthenticationApi.countHitToken(Env.getContext(Env.getCtx(), Constants.CTX_TOKEN));
						filterChain.doFilter(servletRequest, response);
					}else{
						if (servletResponse instanceof HttpServletResponse) {
							HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
							httpServletResponse
									.setStatus(HttpServletResponse.SC_UNAUTHORIZED); //401
						}
					}
				}else{
					if (servletResponse instanceof HttpServletResponse) {
						HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
						httpServletResponse
								.setStatus(HttpServletResponse.SC_UNAUTHORIZED); //401
					}
				}
				
				
			}
			
			
		}
	}
	
	

}
