package forca.sisi.api.model;

import java.sql.ResultSet;
import java.util.Properties;

public class MWSToken extends X_WS_Token{

	private static final long serialVersionUID = 1L;

	public MWSToken(Properties ctx, int WS_Token_ID, String trxName) {
		super(ctx, WS_Token_ID, trxName);
	}

	public MWSToken(Properties ctx, ResultSet rs, String trxName) {
		super(ctx, rs, trxName);
	}

}
