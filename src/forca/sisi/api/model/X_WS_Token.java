/******************************************************************************
 * Product: iDempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2012 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software, you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY, without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program, if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
/** Generated Model - DO NOT CHANGE */
package forca.sisi.api.model;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Properties;
import org.compiere.model.*;
import org.compiere.util.Env;

/** Generated Model for WS_Token
 *  @author iDempiere (generated) 
 *  @version Release 3.1 - $Id$ */
public class X_WS_Token extends PO implements I_WS_Token, I_Persistent 
{

	/**
	 *
	 */
	private static final long serialVersionUID = 20171204L;

    /** Standard Constructor */
    public X_WS_Token (Properties ctx, int WS_Token_ID, String trxName)
    {
      super (ctx, WS_Token_ID, trxName);
      /** if (WS_Token_ID == 0)
        {
        } */
    }

    /** Load Constructor */
    public X_WS_Token (Properties ctx, ResultSet rs, String trxName)
    {
      super (ctx, rs, trxName);
    }

    /** AccessLevel
      * @return 3 - Client - Org 
      */
    protected int get_AccessLevel()
    {
      return accessLevel.intValue();
    }

    /** Load Meta Data */
    protected POInfo initPO (Properties ctx)
    {
      POInfo poi = POInfo.getPOInfo (ctx, Table_ID, get_TrxName());
      return poi;
    }

    public String toString()
    {
      StringBuffer sb = new StringBuffer ("X_WS_Token[")
        .append(get_ID()).append("]");
      return sb.toString();
    }

	public org.compiere.model.I_AD_Role getAD_Role() throws RuntimeException
    {
		return (org.compiere.model.I_AD_Role)MTable.get(getCtx(), org.compiere.model.I_AD_Role.Table_Name)
			.getPO(getAD_Role_ID(), get_TrxName());	}

	/** Set Role.
		@param AD_Role_ID 
		Responsibility Role
	  */
	public void setAD_Role_ID (int AD_Role_ID)
	{
		if (AD_Role_ID < 0) 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_Role_ID, Integer.valueOf(AD_Role_ID));
	}

	/** Get Role.
		@return Responsibility Role
	  */
	public int getAD_Role_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_Role_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_AD_User getAD_User() throws RuntimeException
    {
		return (org.compiere.model.I_AD_User)MTable.get(getCtx(), org.compiere.model.I_AD_User.Table_Name)
			.getPO(getAD_User_ID(), get_TrxName());	}

	/** Set User/Contact.
		@param AD_User_ID 
		User within the system - Internal or Business Partner Contact
	  */
	public void setAD_User_ID (int AD_User_ID)
	{
		if (AD_User_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_AD_User_ID, Integer.valueOf(AD_User_ID));
	}

	/** Get User/Contact.
		@return User within the system - Internal or Business Partner Contact
	  */
	public int getAD_User_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_AD_User_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	public org.compiere.model.I_C_BPartner getC_BPartner() throws RuntimeException
    {
		return (org.compiere.model.I_C_BPartner)MTable.get(getCtx(), org.compiere.model.I_C_BPartner.Table_Name)
			.getPO(getC_BPartner_ID(), get_TrxName());	}

	/** Set Business Partner .
		@param C_BPartner_ID 
		Identifies a Business Partner
	  */
	public void setC_BPartner_ID (int C_BPartner_ID)
	{
		if (C_BPartner_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_C_BPartner_ID, Integer.valueOf(C_BPartner_ID));
	}

	/** Get Business Partner .
		@return Identifies a Business Partner
	  */
	public int getC_BPartner_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_C_BPartner_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Error Request.
		@param ErrorRequest 
		The Calculation Error of Request API
	  */
	public void setErrorRequest (BigDecimal ErrorRequest)
	{
		set_Value (COLUMNNAME_ErrorRequest, ErrorRequest);
	}

	/** Get Error Request.
		@return The Calculation Error of Request API
	  */
	public BigDecimal getErrorRequest () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_ErrorRequest);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Expired Date.
		@param ExpiredDate Expired Date	  */
	public void setExpiredDate (Timestamp ExpiredDate)
	{
		set_Value (COLUMNNAME_ExpiredDate, ExpiredDate);
	}

	/** Get Expired Date.
		@return Expired Date	  */
	public Timestamp getExpiredDate () 
	{
		return (Timestamp)get_Value(COLUMNNAME_ExpiredDate);
	}

	/** Set Hit.
		@param Hit 
		The Calculation Record of Request API 
	  */
	public void setHit (BigDecimal Hit)
	{
		set_Value (COLUMNNAME_Hit, Hit);
	}

	/** Get Hit.
		@return The Calculation Record of Request API 
	  */
	public BigDecimal getHit () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Hit);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	public org.compiere.model.I_M_Warehouse getM_Warehouse() throws RuntimeException
    {
		return (org.compiere.model.I_M_Warehouse)MTable.get(getCtx(), org.compiere.model.I_M_Warehouse.Table_Name)
			.getPO(getM_Warehouse_ID(), get_TrxName());	}

	/** Set Warehouse.
		@param M_Warehouse_ID 
		Storage Warehouse and Service Point
	  */
	public void setM_Warehouse_ID (int M_Warehouse_ID)
	{
		if (M_Warehouse_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_M_Warehouse_ID, Integer.valueOf(M_Warehouse_ID));
	}

	/** Get Warehouse.
		@return Storage Warehouse and Service Point
	  */
	public int getM_Warehouse_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_M_Warehouse_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Success.
		@param Success Success	  */
	public void setSuccess (BigDecimal Success)
	{
		set_Value (COLUMNNAME_Success, Success);
	}

	/** Get Success.
		@return Success	  */
	public BigDecimal getSuccess () 
	{
		BigDecimal bd = (BigDecimal)get_Value(COLUMNNAME_Success);
		if (bd == null)
			 return Env.ZERO;
		return bd;
	}

	/** Set Webservice Token.
		@param WS_Token_ID Webservice Token	  */
	public void setWS_Token_ID (int WS_Token_ID)
	{
		if (WS_Token_ID < 1) 
			set_ValueNoCheck (COLUMNNAME_WS_Token_ID, null);
		else 
			set_ValueNoCheck (COLUMNNAME_WS_Token_ID, Integer.valueOf(WS_Token_ID));
	}

	/** Get Webservice Token.
		@return Webservice Token	  */
	public int getWS_Token_ID () 
	{
		Integer ii = (Integer)get_Value(COLUMNNAME_WS_Token_ID);
		if (ii == null)
			 return 0;
		return ii.intValue();
	}

	/** Set Token.
		@param WS_Token_UU Token	  */
	public void setWS_Token_UU (String WS_Token_UU)
	{
		set_Value (COLUMNNAME_WS_Token_UU, WS_Token_UU);
	}

	/** Get Token.
		@return Token	  */
	public String getWS_Token_UU () 
	{
		return (String)get_Value(COLUMNNAME_WS_Token_UU);
	}
}