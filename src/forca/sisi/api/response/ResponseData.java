package forca.sisi.api.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
//import javax.xml.bind.annotation.XmlElementWrapper;
//import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "responsedata")
public class ResponseData {
	List<Object> resultdata = new ArrayList<Object>();
	
	//E=Error or S=Sukses
	String codestatus = "E";

	//Boolean failed = true;
	String message = "Failed Error Found !!";
	
	@XmlElement(name = "codestatus")
	public String getCodestatus() {
		return codestatus;
	}

	public void setCodestatus(String codestatus) {
		this.codestatus = codestatus;
	}
		
//	@XmlElement(name = "failed")
//	public Boolean getFailed() {
//		return failed;
//	}
//
//	public void setFailed(Boolean failed) {
//		this.failed = failed;
//	}

	@XmlElement(name = "message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	//@XmlElementWrapper(name = "listdata")
	@XmlElement(name = "resultdata")
	public List<Object> getResultdata() {
		return resultdata;
	}

	public void setResultdata(List<Object> resultdata) {
		this.resultdata = resultdata;
	}
	
}
