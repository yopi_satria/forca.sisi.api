package forca.sisi.api.util;

public class Constants {
	//  Variable Names
	public static final String		P_USERNAME      = "User";
	public static final String		P_PASSWORD      = "Password";
	//private static final String		P_SUBMIT        = "Submit";
	public static final String		P_REMEMBER      = "RememberMe";
	
	//  Define Idempiere
	public static final String   P_USER          = "AD_User_ID"; 
	public static final String   P_ROLE          = "AD_Role_ID";
	public static final String   P_CLIENT        = "AD_Client_ID";
	public static final String   P_ORG           = "AD_Org_ID";
	public static final String   P_DATE          = "Date";
	public static final String   P_WAREHOUSE     = "M_Warehouse_ID";
	public static final String   P_ERRORMSG      = "ErrorMessage";
	public static final String   P_STORE         = "SaveCookie";
	public static final String	 P_LANGUAGE		= "Language";
	public static String APP_USER = "";
	
	//Define Context
	public static final String   CTX_USER          = "#AD_User_ID"; 
	public static final String   CTX_CLIENT        = "#AD_Client_ID";
	public static final String   CTX_ROLE          = "#AD_Role_ID";
	public static final String   CTX_ORG           = "#AD_Org_ID";
	public static final String   CTX_DATE          = "Date";
	public static final String   CTX_WAREHOUSE     = "#M_Warehouse_ID";
	public static final String   CTX_USER_NAME     = "#AD_User_Name"; 
	public static final String   CTX_SALESREP      = "#SalesRep_ID"; 
	public static final String   CTX_LANGUAGE      = "#AD_Language"; 
	public static final String   CTX_TOKEN         = "#WS_Token"; 
	public static final String   CTX_BPARTNER      = "#C_BPartner_ID";
	
	//Define Authentication Header
	public static final String AUTH_HEADER = "Authorization";
	public static final String AUTH_FORCA_TOKEN = "Forca-Token";
	
	public static final int   SLEEP_TIME     = 2000;
}
