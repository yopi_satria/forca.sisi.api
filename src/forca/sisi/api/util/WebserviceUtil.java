package forca.sisi.api.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class WebserviceUtil {

	public WebserviceUtil() {
		
	}
	
	public static Timestamp stringToTimeStamp(String sdate){
		Timestamp date = null;
		try {
			if(sdate != null && !sdate.equals("") )
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				Date parsedTimeStamp = dateFormat.parse(sdate);
				date = new Timestamp(parsedTimeStamp.getTime());			
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;		
	}
}
