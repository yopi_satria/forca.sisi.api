package forca.sisi.api.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.compiere.model.MBPGroup;
import org.compiere.model.MPriceList;
import org.compiere.model.MRefList;
import org.compiere.model.MWarehouse;
import org.compiere.model.Query;
import org.compiere.util.Env;

public class WsUtil {
	static DateFormat formatter;
	  public static String SERVER_KEY = "AIzaSyDwJUUZxDHDK5j4Lr_RZjMJT14SVcEDIks";
	  public static String FCM_URL = "https://fcm.googleapis.com/fcm/send";
	  
	  public static Timestamp convertFormatTgl(String tgle)
	  {
	    formatter = new SimpleDateFormat("yyyy-MM-dd");
	    Timestamp xtgl = null;
	    try
	    {
	      Date date = formatter.parse(tgle);
	      xtgl = new Timestamp(date.getTime());
	    }
	    catch (ParseException e)
	    {
	      e.printStackTrace();
	    }
	    return xtgl;
	  }
	  
	  public static Boolean convertYesNo(String val)
	  {
	    if (val.equalsIgnoreCase("N")) {
	      return Boolean.valueOf(false);
	    }
	    return Boolean.valueOf(true);
	  }
	  
	  public static int defaultBPGroup(Properties ctx)
	  {
	    int c_bpgrup_id = 0;
	    String prmtr = "AD_Client_ID = " + Env.getAD_Client_ID(ctx) + " AND name like 'Standard'";
	    Query q = new Query(Env.getCtx(), "C_BP_Group", prmtr, null);
	    
	    MBPGroup bpgrup = (MBPGroup)q.first();
	    if (bpgrup.getC_BP_Group_ID() > 0) {
	      c_bpgrup_id = bpgrup.getC_BP_Group_ID();
	    }
	    return c_bpgrup_id;
	  }
	  
	  public static int defaultWarehouse(Properties ctx)
	  {
	    int whse_id = 0;
	    String prmtr = "AD_Client_ID = ? AND name=? ";
	    Query q = new Query(Env.getCtx(), "M_Warehouse", prmtr, null);
	    q.setParameters(new Object[] { Integer.valueOf(Env.getAD_Client_ID(ctx)), "Standard" });
	    q.setOnlyActiveRecords(true);
	    MWarehouse whse = (MWarehouse)q.first();
	    if (whse.getM_Warehouse_ID() > 0) {
	      whse_id = whse.getM_Warehouse_ID();
	    }
	    return whse_id;
	  }
	  
	  public static int defaultWarehouseSISI(Properties ctx)
	  {
	    int whse_id = 0;
	    String prmtr = "AD_Client_ID = ? AND name=? ";
	    Query q = new Query(Env.getCtx(), "M_Warehouse", prmtr, null);
	    q.setParameters(new Object[] { Integer.valueOf(Env.getAD_Client_ID(ctx)), "SISI" });
	    q.setOnlyActiveRecords(true);
	    MWarehouse whse = (MWarehouse)q.first();
	    if (whse.getM_Warehouse_ID() > 0) {
	      whse_id = whse.getM_Warehouse_ID();
	    }
	    return whse_id;
	  }
	  
	  public static int defaultPriceJual(Properties ctx)
	  {
	    int prc_id = 0;
	    String prmtr = "AD_Client_ID = ? and IsSOPriceList=? ";
	    Query q = new Query(Env.getCtx(), "M_PriceList", prmtr, null);
	    q.setParameters(new Object[] { Integer.valueOf(Env.getAD_Client_ID(ctx)), "Y" });
	    q.setOnlyActiveRecords(true);
	    MPriceList prc = (MPriceList)q.first();
	    if (prc.getM_PriceList_ID() > 0) {
	      prc_id = prc.getM_PriceList_ID();
	    }
	    return prc_id;
	  }
	  
	  public static String descDocStatus(Properties ctx, String kode)
	  {
	    String ref_desc = "";
	    
	    String prmtr = "AD_Client_ID = ? and AD_Reference_ID=? and value = ? ";
	    Query q = new Query(Env.getCtx(), "AD_Ref_List", prmtr, null);
	    q.setParameters(new Object[] { Integer.valueOf(0), Integer.valueOf(131), kode });
	    q.setOnlyActiveRecords(true);
	    MRefList prc = (MRefList)q.first();
	    if (prc.getAD_Ref_List_ID() > 0) {
	      ref_desc = prc.getName();
	    }
	    return ref_desc;
	  }
}
